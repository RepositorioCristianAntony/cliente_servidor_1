<?php
use proyectoCS\modelos\Lesiones;

include "modelos\Conexion.php";
include "modelos\Lesiones.php";


class LesionesController
 	{

 	public $id_Lesion;
	public $id_Usuario;
	public $id_Denuncia;
	public $armaUtilizada;
	public $ubicacionDeHeridas;
	public $atencionMedica;
	public $gravedad;
	public $diasAM;
	public $diasReposo;
	public $tratamiento;

	function crear()
	{
		if(isset($_GET)) {
			echo "Registrado";
			$lesiones = new \modelos\Lesiones();

			$lesiones->id_Usuario = $_GET["id_Usuario"];
			$lesiones->id_Denuncia = $_GET["id_Denuncia"];
			$lesiones->armaUtilizada = $_GET["armaUtilizada"];
			$lesiones->ubicacionDeHeridas = $_GET["ubicacion"];
			$lesiones->atencionMedica = $_GET["atencionMedica"];
			$lesiones->gravedad = $_GET["gravedad"];
			$lesiones->diasAM = $_GET["diasAM"];
			$lesiones->diasReposo = $_GET["diasReposo"];
			$lesiones->tratamiento = $_GET["tratamiento"];

			$lesiones->insert();
			echo json_encode(["status" => "success", "Lesiones" => $lesiones]);
		}
	}

	function buscarID()
	{
			if(isset($_GET)) {
			$buscar = $_GET["id_Lesion"];
			echo json_encode(\modelos\Lesiones::findID($buscar));
		}



	}

	function buscarDenuncia()
	{
		if(isset($_GET)) {
			$denuncia = $_GET["id_Denuncia"];
			echo json_encode(\modelos\Lesiones::findDenuncia($denuncia));
		}
				
		
        
	}

	function mostrar()
	{
		echo json_encode(\modelos\Lesiones::all());
	}

	function actualizar()
	{
		if(isset($_GET)) {
			$lesiones = new \modelos\Lesiones();

			$lesiones->id_Usuario = $_GET["id_Usuario"];
			$lesiones->id_Denuncia = $_GET["id_Denuncia"];
			$lesiones->armaUtilizada = $_GET["armaUtilizada"];
			$lesiones->ubicacionDeHeridas = $_GET["ubicacion"];
			$lesiones->atencionMedica = $_GET["atencionMedica"];
			$lesiones->gravedad = $_GET["gravedad"];
			$lesiones->diasAM = $_GET["diasAM"];
			$lesiones->diasReposo = $_GET["diasReposo"];
			$lesiones->tratamiento = $_GET["tratamiento"];
			$lesiones->id_Lesion = $_GET["id_Lesion"];

			$lesiones->update();
			echo json_encode(["status" => "success", "Lesiones" => $lesiones]);
		}
	}

	function eliminar(){
		if(isset($_GET)) {
			$eliminar = $_GET["id_Lesion"];
			echo json_encode(\modelos\Lesiones::delete($eliminar));

		}
	}

}
?>
<?php
use proyectoCS\modelos\Denuncia;

include "modelos\Conexion.php";
include "modelos\Denuncia.php";


class DenunciaController
 	{

 	public $id_Denuncia;
	public $id_Usuario; 
	public $fecha;
	public $Lugar;	
	public $direccion;
	public $Horario;
	public $Descripcion;
	public $Evidencia;

	function crear()
	{
		if(isset($_GET)) {
			echo "Registrado";
			$denuncia = new \modelos\Denuncia();

			$denuncia->id_Usuario = $_GET["id_Usuario"];
			$denuncia->fecha = $_GET["fecha"];
			$denuncia->Lugar = $_GET["lugar"];
			$denuncia->direccion = $_GET["direccion"];
			$denuncia->Horario = $_GET["horario"];
			$denuncia->Descripcion = $_GET["descripcion"];
			$denuncia->Evidencia = $_GET["evidencia"];
			$denuncia->insert();
			echo json_encode(["status" => "success", "Denuncia" => $denuncia]);
		}
	}

	function buscarID()
	{
			if(isset($_GET)) {
			$buscar = $_GET["id_Denuncia"];
			echo json_encode(\modelos\Denuncia::findID($buscar));

		}

	}

	function buscarLugar()
	{
			if(isset($_GET)) {
			$buscar = $_GET["lugar"];
			echo json_encode(\modelos\Denuncia::findLugar($buscar));

		}


	}

	function mostrar()
	{
		echo json_encode(\modelos\Denuncia::all());
	}

	function actualizar()
	{
		if(isset($_GET)) {
			$denuncia = new \modelos\Denuncia();

			$denuncia->fecha = $_GET["fecha"];
			$denuncia->Lugar = $_GET["lugar"];
			$denuncia->direccion = $_GET["direccion"];
			$denuncia->Horario = $_GET["horario"];
			$denuncia->Descripcion = $_GET["descripcion"];
			$denuncia->Evidencia = $_GET["evidencia"];
			$denuncia->id_Denuncia = $_GET["id_Denuncia"];
			$denuncia->update();
			echo json_encode(["status" => "success", "Denuncia" => $denuncia]);
		}
	}

	function eliminar(){
		if(isset($_GET)) {
			$eliminar = $_GET["id_Denuncia"];
			json_encode(\modelos\Denuncia::delete($eliminar));

		}
	}

}

 
?>
<?php 
namespace modelos;

class Lesiones extends Conexion
{
	public $id_Lesion;
	public $id_Usuario;
	public $id_Denuncia;
	public $armaUtilizada;
	public $ubicacionDeHeridas;
	public $atencionMedica;
	public $gravedad;
	public $diasAM;
	public $diasReposo;
	public $tratamiento;
	
	function insert()
	{
		$pre = mysqli_prepare($this->con, "INSERT INTO lesiones (id_Usuario, id_Denuncia, armaUtilizada, ubicacionDeHeridas, atencionMedica, gravedad, diasAM, diasReposo, tratamiento) VALUES (?,?,?,?,?,?,?,?,?)");
		$pre->bind_param("iisssssss",$this->id_Usuario, $this->id_Denuncia, $this->armaUtilizada, $this->ubicacionDeHeridas, $this->atencionMedica, $this->gravedad, $this->diasAM, $this->diasReposo, $this->tratamiento);
		$pre->execute();
		return true;
	}

	static function findID($id_Lesion)
	{
		$me = new \modelos\Conexion();

		$pre = mysqli_prepare($me->con, "SELECT * FROM lesiones WHERE id_Lesion = ?");
		$pre->bind_param( "i", $id_Lesion);
		$pre->execute();
		$res = $pre->get_result();
		return $res->fetch_object(Lesiones::class);

	}

	static function findDenuncia($id_Denuncia)
	{
		$me = new \modelos\Conexion();

		$pre = mysqli_prepare($me->con, "SELECT * FROM lesiones WHERE id_Denuncia = ?");
		$pre->bind_param( "i", $id_Denuncia);
		$pre->execute();
		$res = $pre->get_result();
		return $res->fetch_all();
	}

	static function all()
	{
		$me = new \modelos\Conexion();

		$pre = mysqli_prepare($me->con, "SELECT * FROM lesiones");
		$pre->execute();
		$res = $pre->get_result();
		return $res->fetch_all();


	}
	function update(){
		$pre = mysqli_prepare($this->con, "UPDATE lesiones SET id_Usuario = ?, id_Denuncia = ?, armaUtilizada = ?, ubicacionDeHeridas = ?, atencionMedica = ?, gravedad = ?, diasAM = ?, diasReposo = ?, tratamiento = ? WHERE id_Lesion = ? ");
		$pre-> bind_param("sssssssssi",$this->id_Usuario, $this->id_Denuncia, $this->armaUtilizada, $this->ubicacionDeHeridas, $this->atencionMedica, $this->gravedad, $this->diasAM, $this->diasReposo, $this->tratamiento, $this->id_Lesion);
		$pre-> execute();
		return true;
	}

	
	static function delete($id_Lesion)
	{
		$me = new \modelos\Conexion();

		$pre = mysqli_prepare($me->con, "DELETE FROM lesiones WHERE id_Lesion = ?");
		$pre->bind_param( "i", $id_Lesion);
		$pre->execute();
		$res = $pre->get_result();
		echo "Eliminado";
	}

}
 	
?>